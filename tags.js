exports.a      = function(a,c){return Tag('a'     , a, c);}
exports.b      = function(a,c){return Tag('b'     , a, c);}
exports.br     = function(a,c){return Tag('br'    , a, c);}
exports.body   = function(a,c){return Tag('body'  , a, c);}
exports.button = function(a,c){return Tag('button', a, c);}
exports.head   = function(a,c){return Tag('head'  , a, c);}
exports.form   = function(a,c){return Tag('form'  , a, c);}
exports.html   = function(a,c){return Tag('html'  , a, c);}
exports.input  = function(a,c){return Tag('input' , a, c);}
exports.li     = function(a,c){return Tag('li'    , a, c);}
exports.meta   = function(a,c){return Tag('meta'  , a, c);}
exports.p      = function(a,c){return Tag('p'     , a, c);}
exports.ul     = function(a,c){return Tag('ul'    , a, c);}

function Tag(name, args, children) {
	var content = (children.length?"\n":"") +children.join("\n")+ (children.length?"\n":"");
	return "<" + name + (args.length > 0 ? " " : "") + args + ">" + content + "</" + name+">";
}
