var tag = require('./tags.js');
var http = require('http');
var querystring = require('querystring');
var mysql = require('mysql');

var connection = mysql.createConnection({
	host: 'localhost',
	user: 'test',
	password: 'password',
	database: 'nodejs_test'
});

var server = http.createServer(function (request, response) {
	var method = request.method.toLowerCase();
	if(method == 'get') {
		console.log("GET");
		handleGet(response);
	} else if (method == 'post') {
		var reply  = "";
		request.on('data', function(chunk) {
			reply += chunk.toString();
		});
		request.on('end', function() {
			console.log("POST: " + reply);
			handlePost(response, reply);
		});
	}
}).listen(8080,function() {
	console.log("Server started...");
});

function buildSQLQuery(fields) {
	var query = "";
	switch(fields.action) {
		case 'delete':
		query = "delete from race_horses where name = '";
		query += fields.name + "';";
		break;

		case 'insert':
		query = "insert into race_horses values (NULL, '";
		query += fields.name + "', ";
		query += fields.position + ");";
		break;
	}
	return query;
}

function performQuery(query, callback) {
	console.log("QUERY: " + query);
	connection.query(query, function(err, rows, fields) {
		if(err) {
			console.log("Error: " + err);
		}
		callback(err, rows, fields);
	});
}

function handlePost(response, reply) {
	var  formData = querystring.parse(reply);
	var query = buildSQLQuery(formData);
	performQuery(query, function() {
		handleGet(response);
	});
}

function handleGet(response){
	response.writeHead(200, {
		'content-type': 'text/html'
	});
	var query = 'select * from race_horses;';
	performQuery(query, function(err, rows) {
		response.end(renderHTML(rows));
	});
}

function renderHTML(horses) {
	var horsesList = [];

	for(horse in horses){
		horsesList.push(
			tag.li('', [horses[horse].id + ": " + horses[horse].name + " - " + horses[horse].position])
		);
	}

	return "<!DOCTYPE html>\n" +
	tag.html('',[
		tag.head('',[
			tag.meta('charset="utf-8"', [])
		]),
		tag.body('', [
			tag.p('',[new Date()]),
			tag.form('method="post"',[
				tag.input('type="text" name="name" placeholder="Namn"',[]),
				tag.input('type="hidden" name="action" value="delete"',[]),
				tag.input('type="submit" value="delete"',[]),
			]),
			tag.form('method="post"',[
				tag.input('type="text" name="name" placeholder="Namn"',[]),
				tag.input('type="text" name="position" placeholder="Position"',[]),
				tag.input('type="hidden" name="action" value="insert"',[]),
				tag.input('type="submit" value="insert"',[]),
			]),
			tag.ul('',horsesList)
		])
	]) + '\n';
}
