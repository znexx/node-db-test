SHELL=/bin/sh
CC=nodejs
CFLAGS=
SRC=main.js

run:
	$(CC) $(SRC)

run_verbose:
	$(CC) $(SRC) -v

clean:
